* https://pkgs.alpinelinux.org/package/edge/main/x86/qemu-system-i386
* https://pkgs.alpinelinux.org/package/edge/main/x86/qemu-system-arm

* https://hub.docker.com/r/multiarch/qemu-user-static/
* https://en.wikipedia.org/wiki/QEMU
* https://en.wikibooks.org/wiki/QEMU

* [wiki.ubuntu.com](https://wiki.ubuntu.com/) > [Base (Ubuntu Base)](https://wiki.ubuntu.com/Base)
* [ubuntu.com](https://www.ubuntu.com/download/alternative-downloads) > [Alternative downloads](https://www.ubuntu.com/download/alternative-downloads) (Check if they are easy to use with Qemu in cli)

* [wiki.alpinelinux.org](https://wiki.alpinelinux.org/) > [*Install Alpine Linux in Qemu*](https://wiki.alpinelinux.org/wiki/Qemu)

### Warning and error messages
* [Warning: requested NIC (anonymous, model unspecified) was not created (not supported by this machine?)](https://google.com/search?q=Warning%3A+requested+NIC+%28anonymous%2C+model+unspecified%29+was+not+created+%28not+supported+by+this+machine%3F%29)
  * Creating two NICs for OpenWrt on ARM

## Similar projects
* [travis-util/qemu](https://github.com/travis-util/qemu)@github
* [gitlab.com/qemu-demo/alpine-virt](https://gitlab.com/qemu-demo/alpine-virt)@gitlab

## Official documentation
* [Platforms/ARM](https://wiki.qemu.org/Documentation/Platforms/ARM)

## Documentation
* [Debian on an emulated ARM machine](https://www.aurel32.net/info/debian_arm_qemu.php)
* [OpenWrt in QEMU](https://openwrt.org/docs/guide-user/virtualization/qemu)